﻿namespace Milestone_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.partNumberText = new System.Windows.Forms.TextBox();
            this.productNameText = new System.Windows.Forms.TextBox();
            this.productWeightText = new System.Windows.Forms.TextBox();
            this.productColorText = new System.Windows.Forms.TextBox();
            this.productFragileText = new System.Windows.Forms.TextBox();
            this.partNumberLabel = new System.Windows.Forms.Label();
            this.productNameLabel = new System.Windows.Forms.Label();
            this.productWeightLabel = new System.Windows.Forms.Label();
            this.productColorLabel = new System.Windows.Forms.Label();
            this.productFragileLabel = new System.Windows.Forms.Label();
            this.getButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // partNumberText
            // 
            this.partNumberText.Location = new System.Drawing.Point(15, 40);
            this.partNumberText.Name = "partNumberText";
            this.partNumberText.Size = new System.Drawing.Size(100, 20);
            this.partNumberText.TabIndex = 0;
            // 
            // productNameText
            // 
            this.productNameText.Location = new System.Drawing.Point(121, 40);
            this.productNameText.Name = "productNameText";
            this.productNameText.Size = new System.Drawing.Size(100, 20);
            this.productNameText.TabIndex = 1;
            // 
            // productWeightText
            // 
            this.productWeightText.Location = new System.Drawing.Point(227, 40);
            this.productWeightText.Name = "productWeightText";
            this.productWeightText.Size = new System.Drawing.Size(100, 20);
            this.productWeightText.TabIndex = 2;
            // 
            // productColorText
            // 
            this.productColorText.Location = new System.Drawing.Point(333, 40);
            this.productColorText.Name = "productColorText";
            this.productColorText.Size = new System.Drawing.Size(100, 20);
            this.productColorText.TabIndex = 3;
            // 
            // productFragileText
            // 
            this.productFragileText.Location = new System.Drawing.Point(439, 40);
            this.productFragileText.Name = "productFragileText";
            this.productFragileText.Size = new System.Drawing.Size(100, 20);
            this.productFragileText.TabIndex = 4;
            // 
            // partNumberLabel
            // 
            this.partNumberLabel.AutoSize = true;
            this.partNumberLabel.Location = new System.Drawing.Point(12, 9);
            this.partNumberLabel.Name = "partNumberLabel";
            this.partNumberLabel.Size = new System.Drawing.Size(66, 13);
            this.partNumberLabel.TabIndex = 5;
            this.partNumberLabel.Text = "Part Number";
            // 
            // productNameLabel
            // 
            this.productNameLabel.AutoSize = true;
            this.productNameLabel.Location = new System.Drawing.Point(118, 9);
            this.productNameLabel.Name = "productNameLabel";
            this.productNameLabel.Size = new System.Drawing.Size(75, 13);
            this.productNameLabel.TabIndex = 6;
            this.productNameLabel.Text = "Product Name";
            // 
            // productWeightLabel
            // 
            this.productWeightLabel.AutoSize = true;
            this.productWeightLabel.Location = new System.Drawing.Point(224, 9);
            this.productWeightLabel.Name = "productWeightLabel";
            this.productWeightLabel.Size = new System.Drawing.Size(81, 13);
            this.productWeightLabel.TabIndex = 7;
            this.productWeightLabel.Text = "Product Weight";
            // 
            // productColorLabel
            // 
            this.productColorLabel.AutoSize = true;
            this.productColorLabel.Location = new System.Drawing.Point(330, 9);
            this.productColorLabel.Name = "productColorLabel";
            this.productColorLabel.Size = new System.Drawing.Size(71, 13);
            this.productColorLabel.TabIndex = 8;
            this.productColorLabel.Text = "Product Color";
            // 
            // productFragileLabel
            // 
            this.productFragileLabel.AutoSize = true;
            this.productFragileLabel.Location = new System.Drawing.Point(436, 9);
            this.productFragileLabel.Name = "productFragileLabel";
            this.productFragileLabel.Size = new System.Drawing.Size(78, 13);
            this.productFragileLabel.TabIndex = 9;
            this.productFragileLabel.Text = "Product Fragile";
            // 
            // getButton
            // 
            this.getButton.Location = new System.Drawing.Point(15, 100);
            this.getButton.Name = "getButton";
            this.getButton.Size = new System.Drawing.Size(75, 23);
            this.getButton.TabIndex = 10;
            this.getButton.Text = "GET";
            this.getButton.UseVisualStyleBackColor = true;
            this.getButton.Click += new System.EventHandler(this.GetButton_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(121, 100);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "SET";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(227, 100);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "CLEAR";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 135);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.getButton);
            this.Controls.Add(this.productFragileLabel);
            this.Controls.Add(this.productColorLabel);
            this.Controls.Add(this.productWeightLabel);
            this.Controls.Add(this.productNameLabel);
            this.Controls.Add(this.partNumberLabel);
            this.Controls.Add(this.productFragileText);
            this.Controls.Add(this.productColorText);
            this.Controls.Add(this.productWeightText);
            this.Controls.Add(this.productNameText);
            this.Controls.Add(this.partNumberText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Inventory Item TEST";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox partNumberText;
        private System.Windows.Forms.TextBox productNameText;
        private System.Windows.Forms.TextBox productWeightText;
        private System.Windows.Forms.TextBox productColorText;
        private System.Windows.Forms.TextBox productFragileText;
        private System.Windows.Forms.Label partNumberLabel;
        private System.Windows.Forms.Label productNameLabel;
        private System.Windows.Forms.Label productWeightLabel;
        private System.Windows.Forms.Label productColorLabel;
        private System.Windows.Forms.Label productFragileLabel;
        private System.Windows.Forms.Button getButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

