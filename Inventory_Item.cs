﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_2
{
    class Inventory_Item
    {
        // create variables to contain properties
        private String _partNumber;
        private String _productName;
        private Double _productWeight;
        private String _productColor;
        private Boolean _productFragile;

        // constructor for the class to set properties to defaults
        public Inventory_Item()
        {
            _partNumber = "";
            _productName = "";
            _productWeight = 1.0;
            _productColor = "";
            _productFragile = false;
        }

        // get/sets for all above properties
        public string PartNumber
        {
            get { return _partNumber; }
            set { _partNumber = value; }
        }
        public String ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }
        public Double ProductWeight
        {
            get { return _productWeight; }
            set { _productWeight = value; }
        }
        public String ProductColor
        {
            get { return _productColor; }
            set { _productColor = value; }
        }
        public Boolean ProductFragile
        {
            get { return _productFragile; }
            set { _productFragile = value; }
        }
    }
}