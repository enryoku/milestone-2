﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // initializing this form creates an instance of Inventory_Item class
        Inventory_Item test_inventory = new Inventory_Item();

        // reset all text box fields to blank for testing inventory item class set actions
        private void Button3_Click(object sender, EventArgs e)
        {
            partNumberText.Text = "";
            productNameText.Text = "";
            productWeightText.Text = "";
            productColorText.Text = "";
            productFragileText.Text = "";
        }

        // set all values from text box fields to appropriate Inventory_Item class properties (this does not check to see if they are the correct values)
        // this is only to test the functionality of the class
        private void Button2_Click(object sender, EventArgs e)
        {

            test_inventory.PartNumber = partNumberText.Text;
            test_inventory.ProductName = productNameText.Text;
            test_inventory.ProductWeight = double.Parse(productWeightText.Text);
            test_inventory.ProductColor = productColorText.Text;
            test_inventory.ProductFragile = Convert.ToBoolean(productFragileText.Text);
        }

        // get all values and populate appropriate text boxes with current values for Inventory_Item class
        private void GetButton_Click(object sender, EventArgs e)
        {
            partNumberText.Text = test_inventory.PartNumber;
            productNameText.Text = test_inventory.ProductName;
            productWeightText.Text = test_inventory.ProductWeight.ToString();
            productColorText.Text = test_inventory.ProductColor;
            productFragileText.Text = test_inventory.ProductFragile.ToString();
        }
    }
}
